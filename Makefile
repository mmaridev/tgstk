

all:

clean:
	debian/rules clean
	rm -rf *.egg-info telegram
	rm -f ../pytg.tar.gz

pytg-download:
	test -e ../pytg.tar.gz || \
		wget -qO- https://api.github.com/repos/python-telegram-bot/python-telegram-bot/releases/latest \
		| grep "browser_download_url.*tar.gz\"" \
		| cut -d : -f 2,3 \
		| tr -d \" \
		| wget -O ../pytg.tar.gz -qi -

pytg: pytg-download
	cd .. && tar -xvf pytg.tar.gz --wildcards python-telegram-bot-*/telegram
	mv ../python-telegram-bot-*/telegram .
