#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='telegramsysadmintoolkit',
      version='0.1',
      author='Marco Marinello',
      author_email='me@marcomarinello.it',
      license='LGPLv3',
      keywords='python telegram monitoring messaging bot',
      description="Easily monitor your server",
      packages=find_packages(exclude=['tests*']),
)
