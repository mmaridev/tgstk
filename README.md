# Telegram Sysadmin Toolkit

[![pipeline status](https://gitlab.com/mmaridev/tgstk/badges/master/pipeline.svg)](https://gitlab.com/mmaridev/tgstk)
[![gpl v.3.0 license](https://img.shields.io/badge/code-GPLv3-blue.svg)](https://www.gnu.org/licenses/#GPL)
[![Gitter](https://badges.gitter.im/tgstk/community.svg)](https://gitter.im/tgstk/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![Website](https://img.shields.io/website?down_color=red&down_message=offline&up_color=brightgreen&up_message=online&url=https%3A%2F%2Ftgstk.org)](https://tgstk.org)
[![Donate](https://img.shields.io/badge/Donate-PayPal-informational.svg)](https://paypal.me/mmaridev)
[![Download latest stable bundled](https://img.shields.io/badge/download-latest%20bundled-brightgreen)](https://gitlab.com/mmaridev/tgstk/-/jobs/artifacts/master/raw/tgstk-bot-bundled.deb?job=debian-build)


A tool that combines the accessibility of an instant messaging client with the complexity of system administration. Telegram Sysadmin Toolkit wants to be an aid of the system administrator to monitor in real time the security and operation of its servers.
The main idea behind this project is to have a command that sends what is passed in standard input through a bot to sysadmin(s).
Developed a solid framework that through python-telegram-bot (https://github.com/python-telegram-bot/python-telegram-bot) connects the server with Telegram we move on to the development of plugins. One of the main features, the reason why this project was born, is to send a message to sysadmin every new root login via ssh, which is done through pam. If configured, the bot can also request with a message via Telegram whether to grant or not to grant access (2FA). However, this tool will provide the bot with many other commands that allow access to other real-time information about the server such as load, disk and network usage, ...
