# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import telegram
from telegram.ext import Updater, CommandHandler
from telegramsysadmintoolkit import get_conf
from telegramsysadmintoolkit.plugins import plugins


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main(func):
    conf = get_conf()
    updater = Updater(conf["bot"]["token"], use_context=True)
    dp = updater.dispatcher

    for p in plugins:
        dp.add_handler(p())

    dp.add_error_handler(error)

    updater.start_polling()
    updater.idle()
