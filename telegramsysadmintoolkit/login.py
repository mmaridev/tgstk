# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
import getpass
import os
import socket
import telegram
from jinja2 import Template
from telegramsysadmintoolkit import get_conf
from telegramsysadmintoolkit.core.msgsender import get_sender
from telegramsysadmintoolkit.core.exceptions import NotAnSshSession
from telegramsysadmintoolkit.core.sshkeys import get_keytable


def get_key_from_log(user, rhost):
    grep = "Accepted publickey for {} from {} port".format(user, rhost)
    now = datetime.now().strftime("%b %d %H:%M")[:-1].lower()
    with os.popen("journalctl -u ssh.service") as log:
        lines = [
            line.strip() for line in log.readlines()
            if grep in line and line.startswith(now)
        ]
    if not lines:
        return None, None
    # try to get the key
    line = lines[-1].split(": RSA ")
    if len(line) < 2:
        return None, None
    kt = get_keytable(user)
    for key in kt:
        if key == line[1]:
            return key, kt[key]


def send_alert(**kw):
    env = {
        a[4:].lower(): os.environ[a]
        for a in os.environ if a.startswith("PAM_")
    }
    if not env:
        raise NotAnSshSession
    if env["type"] != "open_session":
        return
    conf = get_conf()
    send = get_sender(conf["bot"]["token"])
    env["host"] = socket.gethostname()
    env["key"], env["key_owner"] = get_key_from_log(env["user"], env["rhost"])
    with open("/etc/tgstk/messages/login.html") as login_template:
        msg = Template(login_template.read().strip())
    for sysadmin in conf["sysadmins"]:
        env["sysadmin"] = sysadmin.title()
        try:
            send(
                conf["sysadmins"][sysadmin],
                msg.render(**env),
                parse_mode=telegram.ParseMode.HTML
            )
        except:
            pass
