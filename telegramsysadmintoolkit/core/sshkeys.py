# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import base64
import binascii
import hashlib
import re
import sys
import os


# This function is adapted from
# https://gist.github.com/StevenMaude/f054064ede8c9e781ed8
def sha256_fingerprint_from_pub_key(data):
    data = bytes(data, 'ascii')
    digest = hashlib.sha256(binascii.a2b_base64(data)).digest()
    encoded = base64.b64encode(digest).rstrip(b'=')  # ssh-keygen strips this
    return "SHA256:" + encoded.decode('utf-8')


def refactor_line(line):
    line = line.strip().split(" ")
    # structure is supposed to be
    # [options] ssh-* KEY [owner]
    newline = [None, None, None, None]
    if len(line) < 2:
        return newline
    # find the key
    key_idx = 0
    while "ssh-" not in line[key_idx]:
        key_idx += 1
    if line[0] != line[key_idx]:
        newline[0] = line[0]
    if line[-1] != line[key_idx+1]:
        newline[3] = line[-1]
    newline[1] = line[key_idx]
    newline[2] = line[key_idx+1]
    return newline


def get_keytable(user):
    au_keys_path = os.path.join(
        os.path.expanduser('~{}'.format(user)),
        ".ssh/authorized_keys"
    )
    with open(au_keys_path, "r") as authorized_keys:
        au_ks = map(refactor_line, authorized_keys.readlines())
    return {
        sha256_fingerprint_from_pub_key(a[2]): a[3] for a in au_ks
    }
