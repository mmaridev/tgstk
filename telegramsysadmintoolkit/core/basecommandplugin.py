# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import telegram
from telegram import constants
from telegram.ext import CommandHandler
from telegramsysadmintoolkit import get_conf
from telegramsysadmintoolkit.core.exceptions import ImproperlyConfigured


class TgSTkBaseCommandHandler(CommandHandler):
    def __init__(self):
        if not hasattr(self, "command"):
            raise ImproperlyConfigured(
                "{} has no defined command".format(__name__)
            )
        conf = get_conf()
        self.sysadmins = [conf["sysadmins"][k] for k in conf["sysadmins"]]
        super().__init__(self.command, self.run)

    def run(self, *args, **kw):
        if self.check_user(*args, **kw):
            self.handle(*args, **kw)

    def check_user(self, update, context):
        if str(update.message.from_user.id) not in self.sysadmins:
            update.message.reply_text(
                "It seems like you're not an authorized sysadmin of this server."
                " Ask the sysadmin to add your id ({}) to the sysadmins list."
                .format(update.message.from_user.id)
            )
            return False
        return True

    def get_clean_message(self, update):
        msg = update.message.text
        if msg.startswith("/{}".format(self.command[0])):
            # assume there is a space after the command
            msg = msg[len(self.command[0])+2:]
        return msg

    def send_splitted(self, update, msg):
        pre = ""
        for line in msg.split("\n"):
            if len(pre)+len(line) > constants.MAX_MESSAGE_LENGTH:
                update.message.reply_text(pre)
                pre = ""
            pre += "\n{}".format(line)
        update.message.reply_text(pre)
