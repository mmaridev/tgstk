# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import telegramsysadmintoolkit as tgstk
from telegramsysadmintoolkit.core.exceptions import TelegramBotNotInstalled

try:
    import telegram
except ImportError:
    raise TelegramBotNotInstalled


def send_message(token_or_bot, *args, **kwargs):
    bot = token_or_bot
    if type(bot) != telegram.Bot:
        bot = telegram.Bot(token_or_bot)
    bot.sendMessage(*args, **kwargs)


def get_bot(token=None, **kwconf):
    if token == None:
        token = tgstk.get_conf(**kwconf)["bot"]["token"]
    return telegram.Bot(token)


def get_sender(token=None):
    if token == None:
        token = tgstk.get_conf()["bot"]["token"]
    bot = get_bot(token)
    return lambda *a, **kw: send_message(bot, *a, **kw)
