from .start import StartCommandHandler
from .echo import EchoCommandHandler
from .uptime import UptimeCommandHandler
from .munin import MuninCommandHandler

plugins = [
    StartCommandHandler,
    EchoCommandHandler,
    UptimeCommandHandler,
    MuninCommandHandler,
]
