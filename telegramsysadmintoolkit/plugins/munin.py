# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import telegram
from telegramsysadmintoolkit.core.basecommandplugin import TgSTkBaseCommandHandler


class MuninCommandHandler(TgSTkBaseCommandHandler):
    command = "munin"
    # This will be automatically readed from munin configuration
    # in a future version. Even support for multi-host munin server
    # is on the roadmap.
    MUNIN_BASEDIR = "/var/cache/munin/www/localdomain/localhost.localdomain"

    def handle(self, update, context):
        self.build_images_list()
        q = self.get_clean_message(update).strip()
        if q == "":
            msg = "Available stats are: \n{}".format(
                "\n".join(
                    map(lambda s: s.split("/")[-1].split("-")[0], self.images)
                )
            )
            self.send_splitted(update, msg)
        else:
            sent = 0
            day = "day"
            if " " in q:
                if q.split(" ")[1] in ["day", "week", "month", "year"]:
                    day = q.split(" ")[1]
                q = q.split(" ")[0]
            for f in self.images:
                name = f.split("/")[-1].split("-")[0]
                if "*" in q and q.replace("*", "") in f and \
                    f.endswith("-{}.png".format(day)):
                    update.message.reply_photo(photo=open(f, "rb"))
                    sent += 1
                elif q == name and f.endswith("-{}.png".format(day)):
                    update.message.reply_photo(photo=open(f, "rb"))
                    sent += 1
            if sent == 0:
                update.message.reply_text("No stat found for {} {}".format(
                    q, day
                ))

    def build_images_list(self):
        images = []
        for directory in os.walk(self.MUNIN_BASEDIR):
            for f in directory[2]:
                if f.endswith(".png"):
                    images.append(os.path.join(directory[0], f))
        self.images = images
