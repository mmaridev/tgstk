# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import telegram
from telegramsysadmintoolkit.core.basecommandplugin import TgSTkBaseCommandHandler
import socket


class StartCommandHandler(TgSTkBaseCommandHandler):
    command = "start"

    def handle(self, update, context):
            update.message.reply_text(
                "Hi! You're talking with TgSTk on <code>{}</code>".format(
                    socket.gethostname()
                ),
                parse_mode=telegram.ParseMode.HTML
            )
