# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import configparser
import telegramsysadmintoolkit as tgstk
from telegramsysadmintoolkit.core.exceptions import ToolkitMisconfigured


def get_conf(ignore_sysadmins=False):
    conf = configparser.ConfigParser()
    conf.read(tgstk.CONFFILE)

    if not all([conf.has_section(k) for k in tgstk.REQUIRED_CONFS]):
        missings = [
            k for k in tgstk.REQUIRED_CONFS if not conf.has_section(k)
        ]
        if missings == ["sysadmins"] and not ignore_sysadmins:
            raise ToolkitMisconfigured("Configuration file %s has no section %s" %(
                tgstk.CONFFILE, ", ".join(missings)
            ))

    for section in tgstk.REQUIRED_CONFS:
        for option in tgstk.REQUIRED_CONFS[section]:
            if not conf.has_option(section, option):
                raise ToolkitMisconfigured(
                    "Configuration file %s has no option %s.%s" % (
                        tgstk.CONFFILE, section, option
                    )
                )

    return conf
