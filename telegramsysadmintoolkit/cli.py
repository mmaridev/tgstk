# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from telegramsysadmintoolkit import get_conf
from telegramsysadmintoolkit.core.msgsender import get_sender, get_bot
from telegram.ext import Updater, MessageHandler, Filters
import sys, threading


def sendmsg(func, text=None, recipient=None):
    conf = get_conf()
    if not recipient:
        recipient = [conf["sysadmins"][k] for k in conf["sysadmins"]]
    sender = get_sender(conf["bot"]["token"])
    if text:
        msg = text
    else:
        msg = sys.stdin.read().strip()
    for rcpt in recipient:
        sender(rcpt, msg)
    return



class GetID:
    def __init__(self):
        self.bot = get_bot(ignore_sysadmins=True)
        self.upd = Updater(bot=self.bot, use_context=True)
        self.msgs = list()


    def bot_shutdown(self):
        self.upd.stop()
        self.upd.is_idle = False


    def printmsg(self, data, callback):
        print("Got a message from", data.message.from_user.first_name,
            "\n\t@%s" % data.message.from_user.username,
            "\n\tID:", data.message.from_user.id
        )
        print("Now exiting...")
        threading.Thread(target=self.bot_shutdown).start()


    def start(self, **kw):
        print(
            "Now send a message to",
            self.bot.get_me()["first_name"],
            "(@%s)" % self.bot.get_me()["username"]
        )
        self.upd.dispatcher.add_handler(
            MessageHandler(Filters.text, self.printmsg)
        )
        self.upd.start_polling()
        self.upd.idle()
